﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    class BankWork
    {
        public void Run()
        {
            BankManager manager = new BankManager();

            int position, position2;
            int menuPoint;

            while (true)
            {
                Console.Clear();

                manager.PrintAllClients();

                Console.WriteLine("---------------------------------");

                HelperManager.PrintMenu();

                menuPoint = HelperManager.InputIntInRange("Введите пункт меню: ", 0, 4);

                switch (menuPoint)
                {
                    case 1:
                        int numbCard = HelperManager.RandomNumbCard();

                        string name = HelperManager.InputString("Введите имя: ");
                        string surname = HelperManager.InputString("Введите фамилию: ");
                        Console.WriteLine($"Номер карты: {numbCard}");
                        DateTime data = HelperManager.InputDate("Введите дату выдачи карты: ");
                        int balance = HelperManager.InputInt("Введите баланс: ");
                        string status = "Не активна";

                        manager.AddClients(new BankClients(name, surname, numbCard, data, balance, status));
                        break;
                    case 2:
                        position = HelperManager.InputIntInRange("Введите номер клиента: ", 1, manager.CountClient);

                        manager.RemoveClient(position);
                        break;
                    case 3:
                        position = HelperManager.InputIntInRange("Введите номер клиента: ", 1, manager.CountClient);
                        int ans = HelperManager.InputIntInRange("Введите действие(1-активировать карту, 2-деактивировать): ", 1, 2);

                        manager.ActOrDeactCardForClients(position, ans);
                        HelperManager.ActiveCard(ans);

                        Console.ReadKey();
                        break;
                    case 4:
                        position = HelperManager.InputIntInRange("Номер клиента с которого списать деньги: ", 1, manager.CountClient);
                        position2 = HelperManager.InputIntInRange("Номер клиента для перевода: ", 1, manager.CountClient);

                        manager.TransferMoney(position, position2);
                        break;
                    case 0:
                        Environment.Exit(0);
                        break;
                }
            }
        }
    }
}
