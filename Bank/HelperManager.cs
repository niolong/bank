﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    static class HelperManager
    {
        public static void PrintMenu()
        {
            Console.WriteLine("1. Добавить клиента");
            Console.WriteLine("2. Удалить клиента");
            Console.WriteLine("3. Активировать/деактивировать карту");
            Console.WriteLine("4. Перевести деньги другому клиенту");
            Console.WriteLine("0. Выход");
        }

        public static int InputIntInRange(string msg, int min, int max)
        {
            int n;
            bool ans = true;

            do
            {
                Console.WriteLine(msg);
                ans = int.TryParse(Console.ReadLine(), out n);

            } while (ans == false || n < min || n > max);

            return n;
        }

        public static int InputInt(string msg)
        {
            int n;
            bool ans = true;

            do
            {
                Console.WriteLine(msg);
                ans = int.TryParse(Console.ReadLine(), out n);

            } while (ans == false );

            return n;
        }

        public static string InputString(string msg)
        {
            Console.WriteLine(msg);
            return Console.ReadLine();
        }

        public static void ActiveCard(int ans)
        {
            if (ans == 1)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Активна");
                Console.ResetColor();
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Не активна");
                Console.ResetColor();
            }
       
        }

        public static DateTime InputDate(string msg)
        {
            DateTime time;
            bool ans = true;

            do
            {
                Console.WriteLine(msg);
                ans = DateTime.TryParse(Console.ReadLine(), out time);

            } while (ans == false);

            return time;
        }

        public static int RandomNumbCard()
        {
            Random rnd = new Random();
            int numb=rnd.Next(10000,99999);

            return numb;

        }
    }
}
