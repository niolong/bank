﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    class BankManager
    {
        private List<BankClients> clients;

        public BankManager()
        {
            clients = new List<BankClients>();
        }

        public void AddClients(BankClients client)
        {
            clients.Add(client);
        }

        public void RemoveClient(int position)
        {
            int index = position - 1;
            clients.RemoveAt(index);
        }

        public void PrintAllClients()
        {
            for(int i=0;i<clients.Count;i++)
            {
                clients[i].DrawClient(i+1);
            }
        }

        public void ActOrDeactCardForClients(int position,int ans)
        {
            int index = position - 1;
            if (ans==1)
            {
                clients[index].status = "Aктивирована";
            }
            else if(ans == 2)
            {
                clients[index].status = "Деактивирована";
            }
        }
             
        public void TransferMoney(int position,int position2)
        {
            int index = position - 1;
            int index2 = position2 - 1;

            int moneyForTransfer = HelperManager.InputIntInRange("Введите сумму для перевода с карты: ", 0, clients[index].balance);

            clients[index].balance -= moneyForTransfer ;
            clients[index2].balance += moneyForTransfer;
        }

        public int CountClient
        {
            get { return clients.Count; }
        }
    }
}
