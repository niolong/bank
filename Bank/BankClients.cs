﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    class BankClients
    {
        private string name, surname;
        private int numbCard;
        public int balance { get;set; }
        private DateTime givDate;
        public string status { get; set; }

        public BankClients(string name, string surname, int numbCard, DateTime givDate, int balance, string status)
        {
            this.name = name;
            this.surname = surname;
            this.numbCard = numbCard;
            this.givDate = givDate;
            this.balance = balance;
            this.status = status;
        }

        public void DrawClient(int position)
        {
            Console.WriteLine($"Клиент №: {position}");
            Console.WriteLine($"Имя клиента: {name}");
            Console.WriteLine($"Фамилия клиента: {surname}");
            Console.WriteLine($"Номер карты клиента: {numbCard}");
            Console.WriteLine($"Баланс карты: {balance}");
            Console.WriteLine($"Дата получения карты: {givDate}");
            Console.WriteLine($"Статус карты: {status}");

        }

    }
}
